
//url de origem dos assets
//var origin_url = '.';                           //Local
//var origin_url = 'http://35.231.21.246:8080';   //DEV - GOOGLE CLOUD
var origin_url = 'http://vbneck001liv.pontoslivelo.com.br/livelo_chat_viagens';     //PROD 1
// var origin_url = 'http://10.150.16.196:8080';   //HOMOLOGAÇÃO 1
// var origin_url = 'http://10.150.16.197:8080';   //HOMOLOGAÇÃO 2

//endpoint do broker
// var broker_endpoint = 'http://35.196.226.28:8080/conversations/';  //DEV - GOOGLE CLOUD
var broker_endpoint = 'http://VBNEBR001LIV.pontoslivelo.com.br:8080/conversations/';    //PROD 1
// var broker_endpoint = 'http://10.150.6.18:8080/conversations/';    //PROD 2
// var broker_endpoint = 'http://10.150.16.194:8080/conversations/';  //HOMOLOGAÇÃO 1
// var broker_endpoint = 'http://10.150.16.195:8080/conversations/';  //HOMOLOGAÇÃO 2

//codigo do chat executado depois que carregou os elementos
function chatLiveloJQueryCode() {
  'use strict';

  //carrega os estilos do chat
  $('head').append('<link rel="stylesheet" type="text/css" href="'+origin_url+'/css/styles.css" />');
  $('head').append('<link rel="stylesheet" type="text/css" href="https://afeld.github.io/emoji-css/emoji.css" />');
  //carrega script de tagueamento
  $('head').append('<script>window.dataLayer = window.dataLayer || []; window.dataLayer = [{}];</script>');

  //variaveis globais
  var globalDialogue = [];
  var sendInputAble = true;
  var receivemessage = new Audio(origin_url+'/assets/sounds/receivemessage.mp3');
  var sentmessage = new Audio(origin_url+'/assets/sounds/sentmessage.mp3');
  var createChat = true; //para o chat ser construido apenas umas vez
  var human_layout = true; //para transformar para versão atendente apenas uma vez
  var send_email_layout = true;

  //respostas
  var global_gifjson = {
    'answers': [{
      'text': '<img id="chatTypingGif" src="'+origin_url+'/assets/icons/typing.gif" />'
    }],
    'technicalText': 'loading'
  };
  var global_errorMsg = {
    'answers': [
      {
        'text': '<p>Eita, deu ruim... <i class="em em-grimacing"></i></p><br><p>Dá uma olhada na sua internet pra ver se ela tá funcionando direitinho... Se conseguir, navega no nosso site ou no aplicativo também. Lá você consegue fazer suas trocas, transferências e ver seus pedidos...</p><br><p>Se nada der certo, você pode nos procurar todos os dias das 7h às 23h nos telefones abaixo:</p><br><p>3004-8858 (Capitais e regiões metropolitanas)<br>0800-757-8858 (demais regiões)</p>'
      }
    ]
  };
 
  //icons
  var livelo_chat_icon_chat_blue = origin_url+'/assets/icons/menu-chat.svg';
  var livelo_chat_icon_chat_white = origin_url+'/assets/icons/inverse-chat.svg';
  var livelo_chat_icon_like_unselect = origin_url+'/assets/icons/like-Unselected.svg';
  var livelo_chat_icon_like_select = origin_url+'/assets/icons/like-Selected.svg';
  var livelo_chat_icon_dislike_unselect = origin_url+'/assets/icons/dislike-Unselected.svg';
  var livelo_chat_icon_dislike_select = origin_url+'/assets/icons/dislike-Selected.svg';

  //ajax header
  var broker_headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'API-KEY': '771fc720-b23c-11e6-80f5-76304dec7eb7',
    'PROJECT': 'Livelo',
    'CHANNEL': 'Chat',
    'OS': 'Windows',
    'LOCALE': 'pt-BR',
    'USER-REF': '127.0.0.1'
  };

  //lastSessionCode
  var lastSessionCode = '';
  function setLastSessionCode (value) { lastSessionCode = value; }
  function getLastSessionCode () { return lastSessionCode; }

  //sessionCode
  var sessionCode = '';
  function setSessionCode (value) { sessionCode = value; }
  function getSessionCode () { return sessionCode; }
  
  var LiveloBuildMenu = function() {
    var logo = $('<img>').attr({'src': origin_url+'/assets/icons/menu-support.svg',
                                'id' : 'chat-livelo-logo'});
    var logowrap = $('<div>').addClass('lv-chatbox-menu-logo')
      .append(logo)
      .click(buildHtmlChat);
    var html = $('<div>').addClass('lv-chatbox-menu')
      .addClass('lv-chatbox-menu-visible')
      .addClass('gtm-element-event')
      .attr({'data-gtm-event-category':'pontoslivelo:geral',
        'data-gtm-event-action':'click:ajuda-flutuante',
        'data-gtm-event-label':'principal:[[acao]]'})
      .append(logowrap);
    var position = $('#livelo-everis-chat').attr('position');
    if (position) {
      html.attr({'position':position})
    };
    return html;
  }

  var hideChat = (function() {
    'use strict'
    function hideChat() {
      $('.lv-chatbox').removeClass('lv-chatbox-visible');
      $('.lv-chatbox-menu').addClass('lv-chatbox-menu-visible');
      $('#chat-livelo-logo').attr({'src': origin_url+'/assets/icons/menu-support.svg'});
    }
    return hideChat;
  })();

  var LiveloBuildChatHeader = function() {
    var imgLogo = $('<img/>').attr({'src': origin_url+'/assets/icons/livelo_logotipo.svg'})
      .addClass('lv-chatbox-logotipo');
    var imgClose = $('<img/>').attr({'src': origin_url+'/assets/icons/btn-Close.svg'})
      .addClass('lv-chatbox-closebtn')
      .click(hideChat);
    var inner = $('<div>').addClass('inner')
      .append(imgLogo)
      .append(imgClose);
    var html = $('<div>').addClass('lv-chatbox-header')
      .append(inner);
    return html;
  }

  var LiveloBuildChatHeaderTabsTransfHuman = function() {
    var contt_left = $('<p>')
      .append('Agente Digital')
      .click({target: '#lv-header-tab-left-transf-human'},clickTabsActive);
    var wrap_left = $('<div>')
      .addClass('lv-tabs-wrap')
      .addClass('col')
      .addClass('s6')
      .attr({ 'id': 'lv-header-tab-left-transf-human' })
      .append(contt_left);
    var contt_right = $('<p>')
      .append('Atendente')
      .click({target: '#lv-header-tab-right-transf-human'},clickTabsActive);
    var wrap_right = $('<div>')
      .addClass('lv-tabs-wrap')
      .addClass('col')
      .addClass('s6')
      .attr({ 'id': 'lv-header-tab-right-transf-human' })
      .append(contt_right);
    var wrap = $('<div>').addClass('row')
      .append(wrap_left)
      .append(wrap_right);
    var html = $('<div>').addClass('lv-tabs')
      .append(wrap);
    return html;
  }

  var LiveloBuildChatBodyIframeTransfHuman = function(user, email, cpf, protocol) {
    var sendUser = user? '&neoname='+user : '';
    var sendEmail = email? '&neomail='+email : '';
    var sendCpf = '&neoCPF='+cpf;
    var sendProtocol = '&neoprotocol='+protocol;
    var iframe = $('<iframe>').attr(
      {'src':'https://livelo.neoassist.com/?th=chatcentraliframeNeoa'+sendUser+sendEmail+sendCpf+sendProtocol,
      'width':'360',
      'scrolling': 'true',
      'overflow': 'visible'})
      .addClass('lv-chatbox-body-iframe');
    return iframe;
  }

  var LiveloBuildChatHeaderTabsSendEmail = function() {
    var contt_left = $('<p>')
      .append('Agente Digital')
      .click({target: '#lv-header-tab-left-send-email'},clickTabsActive);
    var wrap_left = $('<div>')
      .addClass('lv-tabs-wrap')
      .addClass('col')
      .addClass('s6')
      .attr({ 'id': 'lv-header-tab-left-send-email' })
      .append(contt_left);
    var contt_right = $('<p>')
      .append('E-mail')
      .click({target: '#lv-header-tab-right-send-email'},clickTabsActive);
    var wrap_right = $('<div>')
      .addClass('lv-tabs-wrap')
      .addClass('col')
      .addClass('s6')
      .attr({ 'id': 'lv-header-tab-right-send-email' })
      .append(contt_right);
    var wrap = $('<div>').addClass('row')
      .append(wrap_left)
      .append(wrap_right);
    var html = $('<div>').addClass('lv-tabs')
      .append(wrap);
    return html;
  }

  var LiveloBuildChatBodyIframeSendEmail = function(user, email, cpf, topic, msg) {
    var sendUser = user? '&neoname='+user : '';
    var sendEmail = email? '&neomail='+email : '';
    var sendCpf = '&neoCPF='+cpf;
    var sendTopic = topic? '&neotopic='+topic : '';
    var sendMsg = '&neomsg='+msg;
    var iframe = $('<iframe>').attr(
      {'src':'https://livelo.neoassist.com/?th=tag_centralliveloemailNeoa'+sendUser+sendEmail+sendCpf+sendTopic+sendMsg,
      'width':'360',
      'scrolling': 'true',
      'overflow': 'visible'})
      .addClass('lv-chatbox-body-iframe');
    return iframe;
  }

  var LiveloBuildChatDateDisplay = function () {
    var date = new Date();
    const weekDays = [
      'Domingo',
      'Segunda-feira',
      'Terça-feira',
      'Quarta-feira',
      'Quinta-feira',
      'Sexta-feira',
      'Sábado'
    ];
    const months = [
      'janeiro',
      'fevereiro',
      'março',
      'abril',
      'maio',
      'junho',
      'julho',
      'agosto',
      'setembro',
      'outubro',
      'novembro',
      'dezembro'
    ];
    
    var currentWeekDay = weekDays[date.getDay()];
    var currentDay = date.getDate();
    var currentMonth = months[date.getMonth()];
    var currentYear = date.getFullYear();
    
    var dateDisplay = $('<h6 id="lv-chatbox-date-display">' 
                          + currentWeekDay + ', ' + currentDay + ' de ' 
                          + currentMonth + ' de ' + currentYear + '</h6>');
    return dateDisplay;
  }

  var LiveloBuildChatBody = function () {
    var list = $('<ul>').attr({ 'id': 'lv-messages-list' })
      .addClass('lv-messages-list');
    var inner = $('<div>').addClass('inner')
      .append(list);
    var html = $('<div>').addClass('lv-chatbox-body')
      .attr({ 'id': 'lv-chatbox-body-messages' })
      .append(LiveloBuildChatDateDisplay())
      .append(inner);
    return html;
  }

  var LiveloBuildChatFooter = function () {
    var input = $('<input>').attr({
        'type': 'text',
        'id': 'msg_to_send',
        'name': 'batatinha',
        'autocomplete': 'off',
        'placeholder': 'Mensagem...'
      });
    var send = $('<img/>').attr({
        'id': 'send-msg',
        'src': origin_url+'/assets/icons/btn-Send.svg',
        'alt': 'send-msg',
        'title': 'enviar mensagem'})
      .addClass('lv-chatbox-sendbtn')
      .click(sendMessage);
    var form = $('<form>').addClass('inner')
      .append(input)
      .append(send)
      .submit(sendMessage);
    var html = $('<div>').addClass('lv-chatbox-footer')
      .append(form);
    return html;
  }

  var LiveloBuildChat = function() {
    var html = $('<div>')
      .addClass('lv-chatbox')
      .append(LiveloBuildChatHeader())
      .append(LiveloBuildChatBody())
      .append(LiveloBuildChatFooter());
    var position = $('#livelo-everis-chat').attr('position');
    if (position) {
      html.attr({'position':position})
    }
    return html;
  };

  var isCarousel = function(options) {
    var carousel = false;
    for (var i=0; i < options.length; i++) {
      var item = options[i];
      if (item['type'] == 'carrossel') {
        carousel = true;
      }
    }
    return carousel;
  }

  function answerHasBtnOptions (answer) {
    if (answer['options'] && answer.options.length>0 && !isCarousel(answer.options)) {
      return true;
    }
    return false;
  }

  function technicalTextHasBtnEmail (technicalText) {
    if (technicalText && technicalText['send_email_btn_title']) {
      return true;
    }
    return false;
  }

  function technicalTextHasBtnTransfHuman (technicalText) {
    if (technicalText && technicalText['transf_human_btn_title']) {
      return true;
    }
    return false;
  }

  var LiveloBuildMsgBotButtons = function (answer, session, context) {
    var html = '';
    var technicalText = (answer['technicalText']? answer['technicalText']: {});
    technicalText = jQuery.type(technicalText)==='string'? JSON.parse(technicalText): technicalText;
    var sessionCode = (session? session: '');
    if (answerHasBtnOptions(answer) || technicalTextHasBtnEmail(technicalText) || technicalTextHasBtnTransfHuman (technicalText)) {
      html = $('<ul>').addClass('lv-msg-list').addClass('lv-msg-buttons');
    }
    if (technicalTextHasBtnEmail (technicalText)) {
      var button = $('<button>')
        .addClass('lv-btn')
        .append(technicalText['send_email_btn_title'])
        .click(function() {
          setTabsActive('#lv-header-tab-right-send-email');
        });
      var wrap = $('<li>').append(button);
      html.append(wrap);
    }
    if (technicalTextHasBtnTransfHuman (technicalText)) {
      console.log(technicalText);
      var button = $('<button>')
        .addClass('lv-btn')
        .append(technicalText['transf_human_btn_title'])
        .click(function() {
          setTabsActive('#lv-header-tab-right-transf-human');
        });
      var wrap = $('<li>').append(button);
      html.append(wrap);
    }
    if (answerHasBtnOptions(answer)) {
      for (var i=0; i < answer.options.length; i++) {
        var btn = answer.options[i];
        var button = '';
        if (isLink(btn['text'])) {
          button = $('<a>')
            .attr({'href': btn['text'], 'target': '_blank'});
        } else {
          button = $('<button>')
            .click({btn: btn, context: context, sessionCode:sessionCode, technical: technicalText},sendButtonMessage);
        }
        button.addClass('lv-btn').append(btn['title']);
        var wrap = $('<li>').append(button);
        html.append(wrap);
      }
    }
    return html;
  }

  var getAskedMsg = function(id) {
    var msg = globalDialogue[id -1];
    return ( msg? msg['text'] : '');
  }

  var LiveloBuildMsgBotLike = function (id, technicalText, sessionCode, answered, asked) {
    var html = '';
    var likeable = false;
    if (typeof(technicalText)==='string' && technicalText!== 'loading') {
      var technicalTt = JSON.parse(technicalText);
      likeable = technicalTt['likeable'];
    }
    if (technicalText != 'loading' && likeable==='true') {
      var like = $('<img/>').addClass('lv-msg-bot-like-icon-like')
        .click({like: true, id: id, sessionCode: sessionCode, answered: answered, asked: asked}, evaluateMessage)
        .attr({'src': livelo_chat_icon_like_unselect});
      var unlike = $('<img/>').addClass('lv-msg-bot-like-icon-unlike')
        .click({like: false, id: id, sessionCode: sessionCode, answered: answered, asked: asked}, evaluateMessage)
        .attr({'src': livelo_chat_icon_dislike_unselect});
      var iconswrap = $('<span>').addClass('lv-msg-bot-like-icons')
        .append(like)
        .append(unlike);
      var text = $('<span>').addClass('lv-msg-bot-like-text')
      //   .append('Como você avalia essa resposta?');
      html = $('<div>').addClass('lv-msg-bot-like-wrap')
        .append(text)
        .append(iconswrap);
    }
    return html;
  }

  var pushMessageBot = (function() {
    "use strict"
    function pushMessageBot (dialogue_list, msg_obj) {
      for (var i=0; i<msg_obj.answers.length; i++) {
        var ans = msg_obj.answers[i]; //Cada resposta ele coloca na variável ans
        if (ans['title'] === "1000B_SAUDACAO" || ans['text'] === "1000B_SAUDACAO"|| ans['title'] === "1000A_SAUDACAO" || ans['text'] === "1000A_SAUDACAO" || ans['title'] === "1005_BOAS_VINDAS" || ans['text'] === "1005_BOAS_VINDAS") {
          setSessionCode('');
          msg_obj.context = '';
        }
        var id = globalDialogue.length;
        var wrap = $('<div>').addClass('lv-msg-wrap-bot');
        var msg = $('<p>').append(ans['text']);
        var btns = LiveloBuildMsgBotButtons(ans, msg_obj['sessionCode'], msg_obj['context']);
        var technicalTt = (ans['technicalText']? ans.technicalText : '{}');
        wrap.append(msg)
          .append(btns)
          .append(LiveloBuildMsgBotLike(id, technicalTt, msg_obj['sessionCode'], ans['text'], getAskedMsg(id)));
        var icon = $('<img/>').addClass('lv-msg-icon-livelo')
          .attr({'src': origin_url+'/assets/icons/livelo.png'});
        var content = $('<div>')
          .attr({'id': 'lv-msg-bot-'+id})
          .append(icon)
          .append(wrap);
          var dialogue = $('<li>').append(content);
        dialogue_list.append(dialogue);
      }
    }
    return pushMessageBot;
  })();

  //var LiveloBuildMsgBot = function (obj) {
    // var id = globalDialogue.length;
    //Trecho do Gregório que resolve o bug
    // if (globalDialogue.length >= 2 && obj.answers[0].title === "1000B_SAUDACAO" || obj.answers[0].text === "1000B_SAUDACAO"|| obj.answers[0].title === "1000A_SAUDACAO" || obj.answers[0].text === "1000A_SAUDACAO" || obj.answers[0].title === "1005_BOAS_VINDAS" || obj.answers[0].text === "1005_BOAS_VINDAS") {
    //   setSessionCode('');
    //   obj.sessionCode = '';
    //   obj.context = '';
    // }
    // var wrap = $('<div>').addClass('lv-msg-wrap-bot');
    // var likeText = '';
    // for (var i=0; i<obj.answers.length; i++) {
    //   var ans = obj.answers[i]; //Cada resposta ele coloca na variável ans
    //   var msg = $('<p>').append(ans.text);
    //   var btns = LiveloBuildMsgBotButtons(ans, obj['sessionCode'], obj['context']);
    //   wrap.append(msg).append(btns);
    //   likeText = likeText + (i>0? ',':'') + ans['text'];
    // }
    // var firstAnswer = obj.answers[0];
    // var technicalTt = (firstAnswer['technicalText']? firstAnswer.technicalText : '{}');
    // wrap.append(LiveloBuildMsgBotLike(id, technicalTt, obj['sessionCode'], likeText, getAskedMsg(id)));
    // var icon = $('<img/>').addClass('lv-msg-icon-livelo')
    //   .attr({'src': origin_url+'/assets/icons/livelo.png'});
    // var content = $('<div>')
    //   .attr({'id': 'lv-msg-bot-'+id})
    //   .append(icon)
    //   .append(wrap);
    // var html = $('<li>').append(content);
    // return html;
  //}

  var LiveloBuildMsgClient = function (obj) {
    var msg = $('<p>').append(obj.text);
    var inner = $('<div>').addClass('lv-msg-wrap-client').append(msg);
    var html = $('<li>').append(inner);
    return html;
  }

  var evaluateMessage = (function () {
    if (event.data['context']) {
      console.log(event.data.context);
    }
    "use strict";
    function evaluateMessage (event) {
      console.log(event);
      var id = event.data.id;
      var parent = $('#lv-msg-bot-'+id);
      if (parent.attr('voted')!='voted') {
        var like = event.data.like;
        parent.attr({'voted': 'voted'});
        parent.find('.lv-msg-bot-like-text').html('Muito obrigado pela sua avaliação!');
        if (like) {
          var target = parent.find('.lv-msg-bot-like-icon-like');
          target.attr({'src': livelo_chat_icon_like_select});
        } else {
          var target = parent.find('.lv-msg-bot-like-icon-unlike');
          target.attr({'src': livelo_chat_icon_dislike_select});
        }
        var likeData = {
          'answered': event.data.answered,
          'asked': event.data.asked,
          'evaluation': (like? 1:0),
          //'intent':
        }
        sendLikeToBroker(event.data.sessionCode, likeData);
      }
    }
    return evaluateMessage;
  })();

  var sendLikeToBroker = (function () {
    "use strict";

    function sendLikeToBroker(sessionCode, likeData) {

      //esconde loading feio e intrometido do site da livelo
      $('.ldmain').hide();
      $('.ldimg').hide();

      var endpoint = broker_endpoint + sessionCode + '/satisfaction';
      $.ajaxSetup({
        headers: broker_headers
      });
      $.ajax({
        url: endpoint,
        dataType: 'json',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(likeData),
        success: function (resp) {
          if (!resp) {
            console.log('Ocorreu um erro.');
          }
          //mostra loading do site da livelo
          $('.ldmain').show();
          $('.ldimg').show();
        },
        error: function (err) {
          console.log('AQUI VAI O ERRO: '+err);
          //mostra loading do site da livelo
          $('.ldmain').show();
          $('.ldimg').show();
        }
      });
    }

    return sendLikeToBroker;
  })();

  function contextHasCpf(context) {
    if (context['CustomerId']){ return true; }
    return false;
  }

  function contextHasProtocol(context) {
    if (context['ProtocolId']) {
      return true;
    }
    return false;
  }
    
  function isBotMsg(obj) {
    if (obj['answers']) {
      return true;
    }
    return false;
  }

  function hasContextHasIntent (obj) {
    if (obj['context'] && obj.context['intent']) {
      return true;
    }
    return false;
  }

  function isTransfHuman (obj) {
    // if (obj['answers'] && obj.answers.length > 0) {
    //   for (var i=0; obj.answers.length > i; i++){
    //     var ans = obj.answers[i];
    //     var technicalText = jQuery.type(ans.technicalText)==='string'? JSON.parse(ans.technicalText): ans.technicalText;
    //     if (technicalText && technicalText['transf_human']) {
    //       return true;
    //     }
    //   }
    // }
    if (hasContextHasIntent(obj) && obj.context.intent=='6001_TRANSFERE_ATH') {
      return true;
    }
    return false;
  }

  function isSecondStepTransfHuman (obj) {
    if (hasContextHasIntent(obj) && obj.context.intent=='6001_TRANSFERE_ATH2') {
      return true;
    }
    return false;
  }

  function isSendEmail (obj) {
    if (obj['answers'] && obj.answers.length > 0) {
      for (var i=0; obj.answers.length > i; i++){
        var ans = obj.answers[i];
        var technicalText = jQuery.type(ans.technicalText)==='string'? JSON.parse(ans.technicalText): ans.technicalText;
        if (technicalText && technicalText['send_email']) {
          return true;
        }
      }
    }
    return false;
  }

  function isEscapeFlux (obj) {
    if (hasContextHasIntent(obj) && obj.context.intent=='ESCAPE') {
      return true;
    }
    return false;
  }

  function isNotLoading (obj) {
    if (obj && obj['technicalText']!='loading') {
      return true;
    }
    return false;
  }

  var pushNewMessage = (function () {
    "use strict";
    function pushNewMessage(obj) {
      var messagesList = $('#lv-messages-list');
      if (isBotMsg(obj)) { //mensagem do bot (vinda do broker)
        if (isTransfHuman(obj) || isSecondStepTransfHuman(obj)) { //muda o layout para humano caso tenha cpf no context
          changeLayoutToHuman(obj['context']);
        }
        if (isSendEmail(obj)) {
          changeLayoutToSendEmail(obj['context']);
        }
        if (isTransfHuman(obj)) { //1a etapa da validação
          window.setTimeout(sendMsgToValidateCpf(obj['context']), 1000);
        } else { //mensagem simples do bot
          if (isEscapeFlux (obj)) { setSessionCode(''); } //escapar do fluxo = zerar o sessionCode
          // messagesList.append(LiveloBuildMsgBot(obj));
          pushMessageBot(messagesList, obj);
        }
      } else { //mensagem de cliente (digitada pelo usuário)
        messagesList.append(LiveloBuildMsgClient(obj));
      }
      if (isNotLoading (obj)) { globalDialogue.push(obj); }
      console.log(globalDialogue); //APAGAR ISSO
      scrollBottom();
    }
    return pushNewMessage;
  })();

  var sendMsgToValidateCpf = (function() {
    "use strict";
    function sendMsgToValidateCpf (context){

      var message = {
        'context': getLastContext()
      };

      if (contextHasCpf(context)) {
        message['CustomerIdIn'] = message.context['CustomerId'];
        message['code'] = 'PROTOCOL_REQUEST';
        message['text'] = message.context['CustomerId'];
      } else {
        message['code'] = 'SOLICITACAO_TRANSFERE_ATH';
        message['text'] = '';
      }

      //habilita envio de mensagem
      sendInputAble = true;

      //esconde loading feio e intrometido do site da livelo
      $('.ldmain').hide();
      $('.ldimg').hide();

      sendMsgToBrokerWithSessionCode(message, false);
    }
    return sendMsgToValidateCpf;
  })();

  var removeRightLayout = (function() {
    "use strict";
    function removeRightLayout() {
      human_layout = true;
      send_email_layout = true;
      var iframe = $(".lv-chatbox-body-iframe");
      var tabs = $(".lv-tabs");
      if (iframe) { iframe.remove(); }
      if (tabs) { tabs.remove(); }
    }
    return removeRightLayout;
  })();

  var changeLayoutToHuman = (function () {
    "use strict";
    function changeLayoutToHuman(context) {
      //esconde loading feio e intrometido do site da livelo
      $('.ldmain').hide();
      $('.ldimg').hide();
      if (human_layout && contextHasCpf(context) && contextHasProtocol(context)) {
        removeRightLayout();
        human_layout = false;
        var chatboxBody = $('#lv-chatbox-body-messages');
        chatboxBody.addClass('lv-chatbox-body-tabs');
        $('.lv-chatbox').addClass('lv-chatbox-tabs');
        LiveloBuildChatHeaderTabsTransfHuman().insertAfter($('.lv-chatbox-header'));
        LiveloBuildChatBodyIframeTransfHuman('', '', context.CustomerId, context.ProtocolId).insertAfter(chatboxBody); //'37837928865'; //19473689071
        setTabsActive('#lv-header-tab-left-transf-human');
      }
    };
    return changeLayoutToHuman;
  })();

  var changeLayoutToSendEmail = (function () {
    "use strict";
    function changeLayoutToSendEmail() {
      if (send_email_layout) {
        removeRightLayout();
        send_email_layout = false;
        var chatboxBody = $('#lv-chatbox-body-messages');
        chatboxBody.addClass('lv-chatbox-body-tabs');
        $('.lv-chatbox').addClass('lv-chatbox-tabs');
        LiveloBuildChatHeaderTabsSendEmail().insertAfter($('.lv-chatbox-header'));
        LiveloBuildChatBodyIframeSendEmail().insertAfter(chatboxBody);
        setTabsActive('#lv-header-tab-left-send-email');
      }
    };
    return changeLayoutToSendEmail;
  })();

  var clickTabsActive = (function(){  
    "use strict";
    function clickTabsActive(event) {
      setTabsActive(event.data.target)
    }
    return clickTabsActive;
  })();

  var setTabsActive = (function(){
    "use strict";
    function setTabsActive(target) {
      $('.lv-tabs-wrap').removeClass('active');
      $(target).addClass('active');
      if ((target == '#lv-header-tab-left-transf-human') || (target == '#lv-header-tab-left-send-email') ) {
        $('.lv-chatbox-footer').css({'left': '0', 'opacity': '1'});
        $('.lv-chatbox-body').css({'left': '0', 'opacity': '1'});
        $('.lv-chatbox-body-iframe').css({'left': '100%', 'opacity': '0'});
      }
      if ((target == '#lv-header-tab-right-transf-human') || (target == '#lv-header-tab-right-send-email') ) {
        $('.lv-chatbox-footer').css({'left': '-100%', 'opacity': '0'});
        $('.lv-chatbox-body').css({'left': '-100%', 'opacity': '0'});
        $('.lv-chatbox-body-iframe').css({'left': '0', 'opacity': '1'});
      }
    }
    return setTabsActive;
  })();

  var afterMsgSent = (function(){
    "use strict";
    function afterMsgSent(code) {
      disableTyping();
      $('#msg_to_send').prop('disabled', false);
      sendInputAble = true;
      $('#msg_to_send').focus();

      //mostra loading do site da livelo
      $('.ldmain').show();
      $('.ldimg').show();
  }
    return afterMsgSent;
  })();

  var scrollBottom = (function () {
    "use strict";
    function scrollBottom() {
      var height = $('.lv-chatbox-body .inner').height();
      $('.lv-chatbox-body').animate({scrollTop: height}, 'slow');
    }
    return scrollBottom;
  })();

  var disableTyping = (function () {
    "use strict";
    function disableTyping () {
      $('#chatTypingGif').closest('li').remove();
    }
    return disableTyping;
  })();

  var sendMsgToBrokerWithSessionCode = (function () {
    "use strict";
    function sendMsgToBrokerWithSessionCode(message, push) {
      if (push) {
        pushNewMessage(message);
        sentmessage.play();
      }
      sendMsgToBroker(getSessionCode(), getLastSessionCode(), message);
    };
    return sendMsgToBrokerWithSessionCode;
  })();

  var sendMsgToBroker = (function () {
    "use strict";

    function sendMsgToBroker(sessionCode, lastSessionCode, message) {
      if (sendInputAble) {

        $('#msg_to_send').prop('disabled', true);
        sendInputAble = false;

        pushNewMessage(global_gifjson); //constroi o gif de loading

        var endpoint = broker_endpoint + sessionCode;
        $.ajaxSetup({
          headers: broker_headers
        });

        //esconde loading feio e intrometido do site da livelo
        $('.ldmain').hide();
        $('.ldimg').hide();

        $.ajax({
          url: endpoint,
          dataType: 'json',
          type: 'post',
          contentType: 'application/json; charset=utf-8',
          data: JSON.stringify(message),
          success: function (resp) {
            if (resp) {
              pushNewMessage(resp);
              receivemessage.play();
              setLastSessionCode(getSessionCode());
              setSessionCode(resp['sessionCode']);
            } else {
              console.log('Ocorreu um erro.');
              pushNewMessage(global_errorMsg);
            }
            afterMsgSent(message['code']);
          },
          error: function (err) {
            console.log(err);
            pushNewMessage(global_errorMsg);
            afterMsgSent(message['code']);
          }
        });

      }
    };
    
    return sendMsgToBroker;
  })();

  var isLink = function(string) {
    return (string.substring(0, 4) == 'http');
  }

  var sendButtonMessage = (function() {
    "use strict";
    function sendButtonMessage(event) {
      var technicalText = jQuery.type(event.data.technical)==='string'? JSON.parse(event.data.technical): event.data.technical;
      var btn = event.data.btn;
      var text = isLink(btn['text']) ? '' : btn['text'];
      var sessionCode = event.data.sessionCode;
      var context = event.data.context;
      var message = {
        'text': '',
        'context': context
      };
      if (technicalText['ChatBot_ButtonReturnField']) { //comportamento específico de botão
        context[technicalText.ChatBot_ButtonReturnField] = text;
        message.context = context;
      } else { //comportamento padrão do botão
        message.text = text;
      }
      sendMsgToBroker(sessionCode, sessionCode, message);
    }
    return sendButtonMessage;
  })();

  var getLastContext = function () {
    var lastMsg = globalDialogue[globalDialogue.length - 1];
    var lastcontext = {};
    if (lastMsg && lastMsg['context']) {
      lastcontext = lastMsg['context'];
    }
    return lastcontext;
  }

  function isProtocolRequest (dialogue, val) {
    var position = dialogue.length - val -1;
    var last = dialogue[position];
    if (last && last['answers'] && hasContextHasIntent(last) && last.context.intent == '6001_TRANSFERE_ATH2' && !last.context['ProtocolId']) {
      return true;
    } else if (val>=1) {
      return isProtocolRequest (dialogue, val -1);
    }
    return false;
  }

  var sendMessage = (function () {
    "use strict";
    function sendMessage(msgToSend) {
      var input = $('#msg_to_send');
      var message = {
        'text': (typeof (msgToSend) == 'string'? msgToSend : input.val()),
        'context': getLastContext()
      };
      if(input.val().trim() != '') {
        if (isProtocolRequest(globalDialogue, 1)) {
          message['code'] = 'PROTOCOL_REQUEST';
        }
        sendMsgToBrokerWithSessionCode(message, true);
        input.val('');
      }
    }
    return sendMessage;
  })();

  var hideMenu = (function() {
    'use strict'
    function hideMenu() {
      $('.lv-chatbox-menu').removeClass('lv-chatbox-menu-visible');
    }
    return hideMenu;
  })();

  var buildHtmlChat = (function() {
    'use strict'
    function buildHtmlChat() {
      if (createChat) {
        createChat = false;
        $('#livelo-everis-chat').append(LiveloBuildChat());
        sendMsgToBroker('', '', { 'text': '', 'context': {} });
        $('form').submit(function (event) {
          event.preventDefault(); //prevent from reloading
        });
      }
      $('.lv-chatbox').addClass('lv-chatbox-visible');
      hideMenu();
    }
    return buildHtmlChat;
  })();

  //a linha a seguir eh o que constroi o html do chat
  $('#livelo-everis-chat').append(LiveloBuildMenu());

  // posicionamento da caixa do chatbot - apenas para teste
  $("#esq-cima").click(function() {
    $('.lv-chatbox-menu').attr({'position':'left-top'});
    $('.lv-chatbox').attr({'position':'left-top'});
    $('.lv-phoneinfo').attr({'position':'left-top'});
    $('.lv-emailform').attr({'position':'left-top'});
  });
  $("#esq-baixo").click(function() {
    $('.lv-chatbox-menu').attr({'position':'left-bottom'});
    $('.lv-chatbox').attr({'position':'left-bottom'});
    $('.lv-phoneinfo').attr({'position':'left-bottom'});
    $('.lv-emailform').attr({'position':'left-bottom'});
  });
  $("#dir-cima").click(function() {
    $('.lv-chatbox-menu').attr({'position':'right-top'});
    $('.lv-chatbox').attr({'position':'right-top'});
    $('.lv-phoneinfo').attr({'position':'right-top'});
    $('.lv-emailform').attr({'position':'right-top'});
  });
  $("#dir-baixo").click(function() {
    $('.lv-chatbox-menu').attr({'position':'right-bottom'});
    $('.lv-chatbox').attr({'position':'right-bottom'});
    $('.lv-phoneinfo').attr({'position':'right-bottom'});
    $('.lv-emailform').attr({'position':'right-bottom'});
  });

};

// window.onload = (function() {
(function() {
  if(typeof jQuery=='undefined') {
    var headTag = document.getElementsByTagName("head")[0];
    var jQueryTag = document.createElement('script');
    jQueryTag.type = 'text/javascript';
    jQueryTag.src = origin_url+'/node_modules/jquery/dist/jquery.js';
    jQueryTag.onload = chatLiveloJQueryCode;
    headTag.appendChild(jQueryTag);
  } else {
    chatLiveloJQueryCode();
  }
})();
